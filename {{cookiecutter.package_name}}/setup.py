# -*- coding: utf-8 -*-
# :Project:   {{ cookiecutter.package_name }} -- {{ cookiecutter.package_description }}
# :Created:   {{ cookiecutter.timestamp }}
# :Author:    {{ cookiecutter.author_fullname }} <{{cookiecutter.author_email }}>
# :License:   {{ cookiecutter.license }}
# :Copyright: {{ cookiecutter.copyright }}
#

import os
from codecs import open

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.rst'), encoding='utf-8') as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.rst'), encoding='utf-8') as f:
    CHANGES = f.read()
with open(os.path.join(here, 'version.txt'), encoding='utf-8') as f:
    VERSION = f.read().strip()

setup(
    name="{{ cookiecutter.package_name }}",
    version=VERSION,
    url="{{ cookiecutter.package_url }}",

    description="{{ cookiecutter.package_description }}",
    long_description=README + u'\n\n' + CHANGES,

    author="{{ cookiecutter.author_fullname }}",
    author_email="{{ cookiecutter.author_email }}",

    license="GPLv3+",
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        "Programming Language :: Python",
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        ],
    keywords='',

    packages=find_packages('src'),
    package_dir={'': 'src'},
    namespace_packages=['{{cookiecutter.tool_provider}}', '{{cookiecutter.tool_provider}}.{{cookiecutter.tool_group}}'],

    install_requires=['setuptools'],
    extras_require={'dev': ['metapensiero.tool.bump_version', 'docutils']},
    setup_requires=['pytest-runner'],
    tests_require=['pytest', 'pytest-asyncio'],
)
