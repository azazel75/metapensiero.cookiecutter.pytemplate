.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.cookiecutter.pytemplate
.. :Created:   dom 09 ago 2015 13:50:30 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: Copyright (C) 2015 Lele Gaifax
..

======================================
 metapensiero.cookiecutter.pytemplate
======================================

A cookiecutter__ template to bootstrap a typical `metapensiero` Python package
==============================================================================

__ https://pypi.python.org/pypi/cookiecutter

This is the template that I (will) use to bootstrap a new Python package. Executing
``cookiecutter`` giving as the only arguments either the directory containing this file, or the
URL of its repository (``https://bitbucket.org/lele/metapensiero.cookiecutter.pytemplate``)
will prompt you for a few setting values and then will create an already `boilerplated` Python
package.

Due to ``cookiecutter``'s cli inability to inject a current timestamp in the Jinja context,
there is a ``cc.py`` script that do just that.

If you happen to have a different name than mine, you can `customize`__ your environment, for
example putting something like the following ``YAML`` into your ``~/.cookiecutterrc`` file::

  default_context:
    tool_provider: acme
    author_fullname: Joe Condor
    author_username: joec
    author_email: {{cookiecutter.author_username}}@{{tool_provider}}.us
    package_url: https://github.com/{{cookiecutter.author_username}}/{{cookiecutter.tool_name}}

__ http://cookiecutter.readthedocs.org/en/latest/advanced_usage.html#user-config-0-7-0
