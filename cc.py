#!/usr/bin/env python
# -*- coding: utf-8 -*-
# :Progetto:  metapensiero.cookiecutter.pytemplate
# :Creato:    dom 09 ago 2015 12:57:35 CEST
# :Autore:    Lele Gaifax <lele@metapensiero.it>
# :Licenza:   GNU General Public License version 3 or later
#

import os, sys
from subprocess import check_output
from cookiecutter.main import cookiecutter


template = sys.argv[1] if len(sys.argv) == 2 else os.path.dirname(os.path.abspath(__file__))
# I'm lazy, and the external date emits the localized output I need with effort
timestamp = check_output('date').decode('utf-8').strip()
year = check_output(['date', '+%Y']).decode('utf-8').strip()
cookiecutter(template, extra_context={'timestamp': timestamp, 'year': year})
